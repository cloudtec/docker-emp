# EMP

Adds mariadb to EP image (nginx, php).

## Tags

* latest - testing build, can vary (v1, v2, v3)
* v1 - mysqld ~10.4, php ~7.2, nginx ~1.18, alpine linux 3.12
* v2 - mysqld ~10.5, php ~7.4, nginx ~1.20, alpine linux 3.14
* v3 - mysqld ~10.6, php ~8.1, nginx ~1.20, alpine linux 3.15
* v4 - mysqld ~10.11, php ~8.3, nginx ~1.24, alpine linux 3.19

## ep-image dependancy hierarchy
When updating the ep-image, it is important to also update the other images who were built on top of ep.
The general structure is as follows:
* cloudtecbern/ep
* cloudtecbern/emp        (<- you are here)

### Updating all images in the hiearchy
When updating the ep-image for a specific tag (e.g. master) you should checkout the matching branch, build the image locally
and then update the image on the docker-registry. All images that depend on ep then have to be rebuilt using their own Dockerfiles,
so when updating the ep-image be sure to have the other images listed above available locally.

For every step in the hiearchy, the updating process the goes as follows:
* checkout the branch for the image tag you want to build
``git checkout master``

* build the image locally
``docker build --pull --no-cache --compress -t cloudtecbern/emp:master .``

* push the image to the registry
``docker push cloudtecbern/emp:master``

Also, when building the latest version of an image (currently v3), be sure to also update the "latest"-tag:
``docker tag cloudtecbern/emp:v3 cloudtecbern/emp:latest``
``docker push cloudtecbern/emp:latest``

### Tools for updating dependant images
In all of the image's repositories are Makefiles that define two targets that can be run with ``make {target}``

* update-docker-image, which just updates the current image and pushes it to the registry
* update-lower-in-hierarchy, which updates and pushes all the images that are lower in the hierarchy
