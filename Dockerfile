FROM cloudtecbern/ep:v1

ENV MYSQL_DATABASE changeme
ENV MYSQL_USER changeme
ENV MYSQL_PASSWORD changeme
ENV MYSQL_ROOT_PASSWORD changeme
ENV MYSQL_CHARSET utf8mb4
ENV MYSQL_COLLATION utf8mb4_general_ci
ENV MYSQL_DATA_DIR /var/lib/mysql

# Change these to indicate that mysql-server runs remotely / only mysql-client is used.
# Changing them is only for information/display and has no effect on the emp image itself.
ENV MYSQL_HOST=127.0.0.1
ENV MYSQL_PORT=3306

### DOCKER SETTINGS ####################################################################################################

# uid for the build (check file bottom for entrypoint uid)
USER root

EXPOSE 3306/tcp

### FOLDERS + FILES ####################################################################################################

ADD scripts/healthcheck/* /scripts/healthcheck/
ADD scripts/startup/* /scripts/startup/

COPY conf/mariadb-server.cnf /etc/my.cnf.d/mariadb-server.cnf
COPY conf/mariadb /etc/init.d/mariadb

RUN chmod -R 0755 /scripts \
  && chmod 0755 /etc/init.d/mariadb \
  && mkdir /var/log/mariadb \
  && chown default:default /var/log/mariadb \
  && mkdir /run/mysqld \
  && chown default:default /run/mysqld \
  \
  \
### INSTALL PACKAGES ###################################################################################################
  && apk add --update --no-cache \
     mariadb mariadb-client mariadb-server-utils mariadb-backup \
  && chown -R default:default $MYSQL_DATA_DIR

# set default uid/gid for container entrypoint and shell
USER default:default

# declare external volume (mysql data folder)
VOLUME $MYSQL_DATA_DIR