# Makefile assumes that all related images are available in the parent-directory
.PHONY: update-higher-in-hierarchy-emp update-docker-image
SHELL = /bin/bash

update-higher-in-hierarchy-emp:
	make update-docker-image
	cd ../docker-empx/ && git checkout master && make update-higher-in-hierarchy-empx

update-docker-image:
	@echo "---updating cloudtecbern/emp:master"
	docker build --pull --no-cache --compress -t cloudtecbern/emp:master .
	docker push cloudtecbern/emp:master
