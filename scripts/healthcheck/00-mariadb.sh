#!/bin/bash

# return exit code 1 if mysql daemon is not running.
pidof mysqld > /dev/null || return 1