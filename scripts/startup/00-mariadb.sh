#!/bin/bash

#
# Run MariaDB daemon in background.
#
# Documentation and original MariaDB init scripts:
# - https://raw.githubusercontent.com/yobasystems/alpine-mariadb/master/alpine-mariadb-amd64/files/run.sh
# - https://wiki.alpinelinux.org/wiki/MariaDB
#

# initialize databases and users (in case data folder is empty)
if [[ -d ${MYSQL_DATA_DIR}/mysql ]]; then
    echo "[i] MySQL directory already present, skipping creation"
    export MYSQL_INIT_PROCESS='existing'
else
    echo "[i] MySQL data directory not found, creating initial DBs"
    export MYSQL_INIT_PROCESS='new'
    mysql_install_db --skip-test-db --skip-networking=1 --skip-name-resolve --datadir=${MYSQL_DATA_DIR}

    MYSQL_INIT_SQL=`mktemp`
    if [[ ! -f "${MYSQL_INIT_SQL}" ]]; then
        echo "[ERROR] Could not create temp file"
        return 1
    fi

    cat << EOF > ${MYSQL_INIT_SQL}
USE mysql;
FLUSH PRIVILEGES ;
GRANT ALL ON *.* TO 'root'@'%' identified by '${MYSQL_ROOT_PASSWORD}' WITH GRANT OPTION ;
GRANT ALL ON *.* TO 'root'@'localhost' identified by '${MYSQL_ROOT_PASSWORD}' WITH GRANT OPTION ;
SET PASSWORD FOR 'root'@'%'=PASSWORD('${MYSQL_ROOT_PASSWORD}') ;
DELETE FROM mysql.user WHERE User='' ;
EOF

    # default database $MYSQL_DATABASE:
    if [[ "${MYSQL_DATABASE}" != "" ]]; then
        echo "[i] Creating database: ${MYSQL_DATABASE}"
        if [[ "${MYSQL_CHARSET}" != "" ]] && [[ "${MYSQL_COLLATION}" != "" ]]; then
            echo "[i] with character set [${MYSQL_CHARSET}] and collation [${MYSQL_COLLATION}]"
            echo "CREATE DATABASE IF NOT EXISTS \`${MYSQL_DATABASE}\` CHARACTER SET ${MYSQL_CHARSET} COLLATE ${MYSQL_COLLATION};" >> ${MYSQL_INIT_SQL}
        else
            echo "[i] with character set: 'utf8' and collation: 'utf8_general_ci'"
            echo "CREATE DATABASE IF NOT EXISTS \`${MYSQL_DATABASE}\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> ${MYSQL_INIT_SQL}
        fi
        if [[ "${MYSQL_USER}" != "" ]]; then
            echo "[i] Creating user: ${MYSQL_USER} with password ${MYSQL_PASSWORD}"
            echo "GRANT ALL ON \`${MYSQL_DATABASE}\`.* to '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';" >> ${MYSQL_INIT_SQL}
            echo "GRANT ALL ON \`${MYSQL_DATABASE}\`.* to '${MYSQL_USER}'@'localhost' IDENTIFIED BY '${MYSQL_PASSWORD}';" >> ${MYSQL_INIT_SQL}
        fi
    fi

    echo "FLUSH PRIVILEGES ;" >> ${MYSQL_INIT_SQL}

    /usr/bin/mysqld --bootstrap --verbose=0 --skip-name-resolve --skip-networking=1 --datadir=${MYSQL_DATA_DIR} < ${MYSQL_INIT_SQL}
    rm -f ${MYSQL_INIT_SQL}

    echo "MySQL init process done. Ready for start up."
fi

/etc/init.d/mariadb start